//
//  RTR_AssignmentTests.swift
//  RTR AssignmentTests
//
//  Created by Gregory Azevedo on 6/9/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import XCTest
@testable import RTR_Assignment

class RTR_AssignmentTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        DataController.data.clear()
        DataController.data.insertDesigners(["Designer 1", "Designer 2", "Designer 3"])
    }
    
    override func tearDown() {
        DataController.data.clear()
        super.tearDown()
    }
    
    func testDesignersCount() {
        let count = DataController.data.numDesigners()
        XCTAssert(count == 3, "has \(count) designers")
    }
    
    func testDesignerName() {
        let d = DataController.data.designerForIndex(0)
        XCTAssert(d?.name == "Designer 1", "first designer's name is \(d?.name)")
    }

    func testLikedDesignersCount() {
        let count = DataController.data.numDesigners()
        XCTAssert(count == 3, "has \(count) designers")
    }
    
    func testToggleLikeWorks() {
        let d1 = DataController.data.designerForIndex(0)
        XCTAssert(d1?.liked == false)
        DataController.data.toggleDesignerLike(d1!, liked: true)
        XCTAssert(d1?.liked == true)
    }
    
    func testLikedDesignerName() {
        DataController.data.toggleDesignerLike(DataController.data.designerForIndex(0)!, liked: true)
        let d = DataController.data.likedDesignerForIndex(0)
        XCTAssert(d?.name == "Designer 1", "first designer's name is \(d?.name)")
    }
    
    func testDesignersSorted() {
        DataController.data.insertDesigners(["DDD", "AAA", "BBB", "ZZZ"])
        for idx in 1 ..< DataController.data.numDesigners() {
            let prevDesigner = DataController.data.designerForIndex(idx - 1)
            let designer = DataController.data.designerForIndex(idx)
            XCTAssert(prevDesigner?.name?.lowercaseString < designer?.name?.lowercaseString, "designers out of order: \(prevDesigner!.name) before \(designer!.name)")
        }
    }
    
    func testLikedDesignersSorted() {
        DataController.data.insertDesigners(["DDD", "AAA", "BBB", "ZZZ"])
        for idx in 1 ..< DataController.data.numDesigners() {
            DataController.data.toggleDesignerLike(DataController.data.designerForIndex(idx)!, liked: true)
        }
        for idx in 1 ..< DataController.data.numLikedDesigners() {
            let prevDesigner = DataController.data.likedDesignerForIndex(idx - 1)
            let designer = DataController.data.likedDesignerForIndex(idx)
            XCTAssert(prevDesigner?.name?.lowercaseString < designer?.name?.lowercaseString, "designers out of order: \(prevDesigner!.name) before \(designer!.name)")
        }
    }
    
    func testDesignerLookupByName() {
        let designer = DataController.data.designerForName("Designer 1")
        XCTAssert(designer != nil)
    }
    
    func testAccessDesignerListOutOfBounds() {
        let nullDesigner = DataController.data.designerForIndex(100)
        XCTAssert(nullDesigner == nil)
    }

}
