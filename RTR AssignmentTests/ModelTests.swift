//
//  ModelTests.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/12/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import XCTest
@testable import RTR_Assignment

class ModelTests: XCTestCase {

    override func setUp() {
        super.setUp()
        DataController.data.clear()
    }
    
    override func tearDown() {
        DataController.data.clear()
        super.tearDown()
    }

    func testDesignerInit() {
        let designer = Designer(name: "Mr Jones")
        XCTAssert(designer.name == "Mr Jones")
    }
    
    func testProductInit() {
        let data = self.stubbedResponse("items")
        let json = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String:AnyObject]
        let list = json["products"] as! [[String:AnyObject]]
        let jsonProduct = list.last!
        let designer = Designer(name: "Joe")
        let product = Product(designer: designer, data: jsonProduct)
        XCTAssert(product.designer!.name == "Joe")
        XCTAssert(product.clearance == (jsonProduct["clearance"] as! Bool))
        XCTAssert(product.displayName == (jsonProduct["displayName"] as! String))
        XCTAssert(product.productDetail == (jsonProduct["productDetail"] as! String))
        XCTAssert(product.styleNotes == (jsonProduct["styleNotes"] as! String))
        XCTAssert(product.rentalFee8Day == (jsonProduct["rentalFee8Day"] as! Int))
        XCTAssert(product.rentalFee == (jsonProduct["rentalFee"] as! Int))
    }

    func testDressInit() {
        let data = self.stubbedResponse("items")
        let json = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String:AnyObject]
        let list = json["products"] as! [[String:AnyObject]]
        let jsonProduct = list.first!
        let designer = Designer(name: "Joe")
        let dress = Dress(designer: designer, data: jsonProduct)
        XCTAssert(dress.fitNotes == (jsonProduct["fitNotes"] as! String))
    }

    func testImportProducts() {
        //importing products requires designers already imported to create relationships
        let ddata = self.stubbedResponse("designers")
        let djson = try! NSJSONSerialization.JSONObjectWithData(ddata, options: .AllowFragments) as! [String:AnyObject]
        let dlist = djson["designer"] as! [String]
        DataController.data.insertDesigners(dlist)

        let data = self.stubbedResponse("items")
        let json = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String:AnyObject]
        let list = json["products"] as! [[String:AnyObject]]
        XCTAssert(list.count == 4) //4 mock items
        DataController.data.insertProductsFromJSON(list)
        var productCount = 0
        for idx in 0 ..< DataController.data.numDesigners() {
            let designer = DataController.data.designerForIndex(idx)
            productCount += designer!.numProducts()
        }
        XCTAssert(productCount == 4, "found \(productCount) products")
    }

    func testImportDesigners() {
        let data = self.stubbedResponse("designers")
        let json = try! NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as! [String:AnyObject]
        let list = json["designer"] as! [String]
        XCTAssert(list.count == 7) //mock data has 7 designers
        DataController.data.insertDesigners(list)
        XCTAssert(DataController.data.numDesigners() == 7)
    }

}
