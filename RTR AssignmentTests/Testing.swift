//
//  Testing.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/12/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import XCTest

extension XCTestCase {
    func stubbedResponse(filename: String) -> NSData! {
        @objc class TestClass: NSObject { }
        let bundle = NSBundle(forClass: TestClass.self)
        let path = bundle.pathForResource(filename, ofType: "json")
        return NSData(contentsOfFile: path!)
    }
}
