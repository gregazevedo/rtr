//
//  Networking.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class Networking {
    static var rootURL = "http://static.sqvr.co"
    
    static func GetDesigners(type: DesignerType, successHandler: ()->()) {
        var url: NSURL!
        switch type {
        case .Dresses: url = NSURL(string: "\(rootURL)/designer-dresses.json")
        case .Accessories: url = NSURL(string: "\(rootURL)/designer-accesories.json")
        }
        defaultRequest(url, success: { (data) in
            do {
                guard let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [String:AnyObject],
                    let list = json["designer"] as? [String] else {
                        //json data not formatted as expected
                        return
                }
                DataController.data.insertDesigners(list)
            } catch {
                //json data not formatted as expected
                return
            }
            successHandler()
            }) { (status, error) in
                //bad response, could add action / spit error based on status code / error
        }
    }
    
    static func GetProducts(successHandler: ()->()) {
        let url = NSURL(string: "\(rootURL)/random-items.json")!
        defaultRequest(url, success: { (data) in
            do {
                guard let json = try NSJSONSerialization.JSONObjectWithData(data, options: .AllowFragments) as? [String:AnyObject],
                    let list = json["products"] as? [[String:AnyObject]] else {
                        //json data not formatted as expected
                        return
                }
                DataController.data.insertProductsFromJSON(list)
            } catch {
                //json data not formatted as expected
                return
            }
            successHandler()
            }) { (status, error) in
                //bad response, could add action / spit error based on status code / error
        }
    }
    
    static func GetImageForURL(url: NSURL, completion: (image: UIImage?)->()) {
        defaultRequest(url, success: { (data) in
            completion(image: UIImage(data: data))
            }) { (status, error) in
                completion(image: nil)
        }
    }
    
    static func defaultRequest(url: NSURL, success:(data: NSData)->(), failure:(status: Int, error: NSError?)->()) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        NSURLSession.sharedSession().dataTaskWithURL(url) { (data, response, error) in
            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            }
            guard let response = response as? NSHTTPURLResponse else {
                return
            }
            guard let data = data where response.statusCode < 400 else {
                failure(status: response.statusCode, error: error)
                return
            }
            success(data: data)
        }.resume()
    }
}
