//
//  ProductDetailViewController.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class ProductDetailViewController: UITableViewController {
    var product: Product!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //dresses also have fit notes
        return (product is Dress) ? 2 : 1
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("NoteCell", forIndexPath: indexPath) as! NoteCell
        cell.titleLabel.text = (indexPath.row == 0) ? "Style Notes" : "Fit Notes"
        cell.contentLabel.text = (indexPath.row == 0) ? product.styleNotes : (product as? Dress)?.fitNotes
        return cell
    }
}
