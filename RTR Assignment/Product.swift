//
//  Product.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class Product {
    var styleName: String?
    var displayName: String?
    var productDetail: String?
    weak var designer: Designer?
    var styleNotes: String?
    var imgURLs: [NSURL] = []
    var rentalFee8Day: Int?
    var rentalFee: Int?
    var clearance: Bool = false
    
    init(designer: Designer, data: [String:AnyObject]) {
        self.designer = designer
        styleName = data["styleName"] as? String
        displayName = data["displayName"] as? String
        productDetail = data["productDetail"] as? String
        styleNotes = data["styleNotes"] as? String
        rentalFee8Day = data["rentalFee8Day"] as? Int
        rentalFee = data["rentalFee"] as? Int
        clearance = data["clearance"] as? Bool ?? false
        if let imgURLs = data["imagesBySize"] as? [String:[String]],
            let urls = imgURLs["270x"] {
            for urlStr in urls {
                if let url = NSURL(string: urlStr) {
                    self.imgURLs.append(url)
                }
            }
        }
    }
}

class Dress: Product {
    var fitNotes: String?
    
    override init(designer: Designer, data: [String : AnyObject]) {
        super.init(designer: designer, data: data)
        fitNotes = data["fitNotes"] as? String
    }
}

class Accessory: Product {
    
}
