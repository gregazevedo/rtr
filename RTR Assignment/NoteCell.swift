//
//  NoteCell.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit
import SnapKit

class NoteCell: UITableViewCell {
    var titleLabel = Label(align: .Left, size: 20)
    var contentLabel = Label(align: .Left)
    private var padding = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        addSubview(titleLabel)
        addSubview(contentLabel)
        addSubview(padding)
        contentLabel.numberOfLines = 0

        titleLabel.snp_makeConstraints { (make) in
            make.top.equalTo(self.snp_top).offset(10)
            make.width.equalTo(self.snp_width).inset(20)
            make.centerX.equalTo(self.snp_centerX)
        }
        contentLabel.snp_makeConstraints { (make) in
            make.top.equalTo(self.titleLabel.snp_bottom).offset(5)
            make.width.equalTo(self.snp_width).inset(20)
            make.centerX.equalTo(self.snp_centerX)
        }
        padding.snp_makeConstraints { (make) in
            make.top.equalTo(self.contentLabel.snp_bottom)
            make.height.equalTo(20)
            make.bottom.equalTo(self.snp_bottom)
        }
    }
}
