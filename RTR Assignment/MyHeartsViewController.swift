//
//  MyHeartsViewController.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class MyHeartsViewController: DesignersViewController {
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataController.data.numLikedDesigners()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DesignerCell", forIndexPath: indexPath) as! DesignerCell
        if let designer = DataController.data.likedDesignerForIndex(indexPath.row) {
            configureCell(cell, withDesigner: designer, index: indexPath.row)
        }
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedDesigner = DataController.data.likedDesignerForIndex(indexPath.row)
        if selectedDesigner?.hasProducts == true {
            performSegueWithIdentifier("ShowDetail", sender: self)
        } else {
            alert("No Products", message: "The selected designer doesn't currently have any products.") { (_) in
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
    }
    
    //MARK: - Actions
    
    override func tappedHeartAtIndex(index: Int?) {
        guard let index = index,
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? DesignerCell else {
            //cell offscreen or failure
            return
        }
        guard let designer = DataController.data.likedDesignerForIndex(index) else {
            return
        }
        let wasLiked = cell.likeButton.liked
        tableView.beginUpdates()
        DataController.data.toggleDesignerLike(designer, liked: !wasLiked)
        tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
        tableView.endUpdates()
        tableView.reloadData() //need to update the cached indexes on the cells
    }
}
