//
//  DesignerCell.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit
import SnapKit

protocol DesignerCellDelegate {
    func tappedHeartAtIndex(index: Int?)
}

class DesignerCell: UITableViewCell {
    var delegate: DesignerCellDelegate?
    let likeButton = LikeButton()
    var index: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textLabel?.textColor = UIColor.coolDarkGrayColor()
        likeButton.addTarget(self, action: #selector(self.tappedHeart), forControlEvents: .TouchUpInside)
        addSubview(likeButton)
        likeButton.snp_makeConstraints { (make) in
            make.trailing.equalTo(self.snp_trailingMargin).inset(28)
            make.centerY.equalTo(self.snp_centerY)
            make.height.equalTo(26)
            make.width.equalTo(30)
        }
    }

    func tappedHeart() {
        delegate?.tappedHeartAtIndex(index)
    }
}
