//
//  ProductCell.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit
import SnapKit

protocol ProductCellDelegate {
    func showNotesForIndex(index: Int?)
}

class ProductCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    var imagesView: UICollectionView!
    var layout = UICollectionViewFlowLayout()
    private var pageControl = UIPageControl()
    var displayNameLabel = Label(align: .Center, size: 22)
    var priceLabel = Label(align: .Center, bold: true)
    var clearanceLabel = Label(align: .Center, size: 12, bold: true)
    var detailLabel = Label(align: .Center)
    var notesButton = UIButton()
    private var padding = UIView()
    var delegate: ProductCellDelegate?
    var index: Int?
    private var images: [UIImage] = []
    var imageURLs: [NSURL] = [] {
        didSet {
            for url in imageURLs {
                Networking.GetImageForURL(url) { (image) in
                    guard let image = image else { return }
                    dispatch_async(dispatch_get_main_queue()) {
                        self.images.append(image)
                        self.imagesView.reloadData()
                        self.pageControl.numberOfPages = self.images.count
                    }
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layout.scrollDirection = .Horizontal
        layout.itemSize = CGSize(width: UIScreen.mainScreen().bounds.width, height: 270)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        imagesView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        imagesView.registerClass(ProductImageCell.self, forCellWithReuseIdentifier: "ProductImageCell")
        imagesView.dataSource = self
        imagesView.delegate = self
        imagesView.backgroundColor = UIColor.coolGrayColor()
        imagesView.pagingEnabled = true
        imagesView.showsHorizontalScrollIndicator = false
        pageControl.currentPageIndicatorTintColor = UIColor.coolDarkGrayColor()
        pageControl.pageIndicatorTintColor = UIColor.coolGrayColor()
        notesButton.setTitle("Item Notes", forState: .Normal)
        notesButton.setTitleColor(UIColor.clickableColor(), forState: .Normal)
        notesButton.setTitleColor(UIColor.coolGrayColor(), forState: .Highlighted)
        notesButton.setTitleColor(UIColor.coolGrayColor(), forState: .Selected)
        notesButton.addTarget(self, action: #selector(self.showNotes), forControlEvents: .TouchUpInside)
        clearanceLabel.text = "CLEARANCE"
        clearanceLabel.layer.cornerRadius = 5
        clearanceLabel.layer.borderColor = UIColor.redColor().CGColor
        clearanceLabel.layer.borderWidth = 2
        clearanceLabel.textColor = UIColor.redColor()
        
        addSubview(imagesView)
        addSubview(pageControl)
        addSubview(displayNameLabel)
        addSubview(priceLabel)
        addSubview(clearanceLabel)
        addSubview(detailLabel)
        addSubview(notesButton)
        addSubview(padding)
        imagesView.snp_makeConstraints { (make) in
            make.top.equalTo(self.snp_top)
            make.leading.equalTo(self.snp_leading)
            make.trailing.equalTo(self.snp_trailing)
            make.height.equalTo(280)
        }
        pageControl.snp_makeConstraints { (make) in
            make.top.equalTo(self.imagesView.snp_bottom).offset(5)
            make.width.equalTo(100)
            make.height.equalTo(20)
            make.centerX.equalTo(self.snp_centerX)
        }
        displayNameLabel.snp_makeConstraints { (make) in
            make.width.equalTo(self.snp_width).inset(40)
            make.top.equalTo(self.pageControl.snp_bottom).offset(20)
            make.centerX.equalTo(self.snp_centerX)
        }
        priceLabel.snp_makeConstraints { (make) in
            make.top.equalTo(self.displayNameLabel.snp_bottom).offset(10)
            make.centerX.equalTo(self.snp_centerX)
        }
        clearanceLabel.snp_makeConstraints { (make) in
            make.leading.equalTo(self.priceLabel.snp_trailing).offset(10)
            make.centerY.equalTo(self.priceLabel.snp_centerY)
            make.width.equalTo(80)
            make.height.equalTo(20)
        }
        detailLabel.snp_makeConstraints { (make) in
            make.width.equalTo(self.snp_width).inset(60)
            make.top.equalTo(self.priceLabel.snp_bottom).offset(20)
            make.centerX.equalTo(self.snp_centerX)
        }
        notesButton.snp_makeConstraints { (make) in
            make.width.equalTo(self.snp_width).inset(40)
            make.top.equalTo(self.detailLabel.snp_bottom).offset(20)
            make.centerX.equalTo(self.snp_centerX)
        }
        padding.snp_makeConstraints { (make) in
            make.top.equalTo(self.notesButton.snp_bottom)
            make.height.equalTo(20)
            make.bottom.equalTo(self.snp_bottom)
        }
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductImageCell", forIndexPath: indexPath) as! ProductImageCell
        cell.imageView.image = images[indexPath.row]
        return cell
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let pageWidth = imagesView.frame.width
        let curPage = Int(round(imagesView.contentOffset.x / pageWidth))
        pageControl.currentPage = curPage
    }
    
    func showNotes() {
        delegate?.showNotesForIndex(index)
    }
}
