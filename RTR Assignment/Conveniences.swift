//
//  Conveniences.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

var ArchiveFilePath : String? {
    let manager = NSFileManager.defaultManager()
    guard let url = manager.URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask).first,
        let path = url.URLByAppendingPathComponent("designers").path else {
            return nil
    }
    return path
}

class Label: UILabel {
    convenience init(align: NSTextAlignment, size: CGFloat = 17, bold: Bool = false) {
        self.init(frame: CGRectZero)
        textAlignment = align
        textColor = UIColor.coolDarkGrayColor()
        font = bold ? UIFont.boldSystemFontOfSize(size) : UIFont.systemFontOfSize(size)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        numberOfLines = 0
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UIColor {
    convenience init(hex:Int, alpha:CGFloat) {
        self.init(red: (CGFloat)((hex & 0xFF0000) >> 16) / 255.0,
                  green: (CGFloat)((hex & 0x00FF00) >> 8) / 255.0,
                  blue: (CGFloat)((hex & 0x0000FF) >> 0) / 255.0,
                  alpha: alpha)
    }
    
    static func coolGrayColor() -> UIColor {
        return UIColor(hex: 0xD1DDDD, alpha: 1)
    }
    
    static func coolDarkGrayColor() -> UIColor {
        return UIColor(hex: 0x455555, alpha: 1)
    }

    static func clickableColor() -> UIColor {
        return UIColor(hex: 0x005EFF, alpha: 1)
    }

}

extension Int {
    func priceString() -> String {
        return "$\(self).00"
    }
}

extension UIViewController {
    func alert(title: String?, message: String?, okHandler: ((UIAlertAction) -> ())? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let ok = UIAlertAction(title: "OK", style: .Default, handler: okHandler)
        alert.addAction(ok)
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

class ActivityIndicator: UIActivityIndicatorView {
    weak var superView: UIView!
    
    init(superView:UIView) {
        super.init(activityIndicatorStyle: UIActivityIndicatorViewStyle.WhiteLarge)
        self.superView = superView
        let bg = UIView(frame: CGRect(x: 0, y: 0, width: 60, height: 60))
        bg.layer.cornerRadius = 5
        bg.backgroundColor = UIColor.lightGrayColor()
        self.color = UIColor.whiteColor()
        bg.center = self.center
        self.addSubview(bg)
        self.sendSubviewToBack(bg)
        self.center = superView.center
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func startAnimating() {
        superView.addSubview(self)
        super.startAnimating()
    }
    
    override func stopAnimating() {
        super.stopAnimating()
        self.removeFromSuperview()
    }
}
