//
//  DesignerDetailViewController.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit
import SnapKit

class DesignerDetailViewController: UITableViewController, ProductCellDelegate {
    var designer: Designer!
    var selectedProduct: Product?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = designer.name
        tableView.estimatedRowHeight = 550
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        //reset in will appear in case like state toggled from another tab
        let heart = UIBarButtonItem(image: UIImage(named:designer.liked ? "selectedHeart" : "unselectedHeart"), style: .Plain, target: self, action: #selector(self.tappedHeart))
        navigationItem.setRightBarButtonItem(heart, animated: false)
    }
    
    func tappedHeart() {
        let heart = UIBarButtonItem(image: UIImage(named:!designer.liked ? "selectedHeart" : "unselectedHeart"), style: .Plain, target: self, action: #selector(self.tappedHeart))
        navigationItem.setRightBarButtonItem(heart, animated: true)
        DataController.data.toggleDesignerLike(designer, liked: !designer.liked)
    }
    
    // MARK: - Table view data source
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return designer.numProducts()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("ProductCell", forIndexPath: indexPath) as! ProductCell
        guard let product = designer.productForIndex(indexPath.row) else {
            return cell
        }
        cell.displayNameLabel.text = product.displayName
        cell.priceLabel.text = product.rentalFee?.priceString()
        cell.detailLabel.text = product.productDetail
        cell.imageURLs = product.imgURLs
        cell.index = indexPath.row //for showing notes
        cell.delegate = self
        cell.clearanceLabel.hidden = !product.clearance
        return cell
    }
    
    //MARK: - Actions
    
    func showNotesForIndex(index: Int?) {
        if let index = index {
            selectedProduct = designer.productForIndex(index)
            performSegueWithIdentifier("ShowDetail", sender: self)
        }
    }

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier {
        case "ShowDetail"?:
            let dest = segue.destinationViewController as! ProductDetailViewController
            dest.product = selectedProduct
        default: break
        }
    }

}
