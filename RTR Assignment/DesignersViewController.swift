//
//  DesignersViewController.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class DesignersViewController: UITableViewController, DesignerCellDelegate {
    var selectedDesigner: Designer?
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData() //update like state when switching tabs
    }
    
    // MARK: - Table view data source

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataController.data.numDesigners()
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("DesignerCell", forIndexPath: indexPath) as! DesignerCell
        if let designer = DataController.data.designerForIndex(indexPath.row) {
            configureCell(cell, withDesigner: designer, index: indexPath.row)
        }
        return cell
    }
    
    func configureCell(cell: DesignerCell, withDesigner designer: Designer, index: Int) {
        cell.textLabel?.text = designer.name
        cell.delegate = self
        cell.index = index //for liking
        cell.likeButton.liked = designer.liked ?? false
        cell.accessoryType = designer.hasProducts ? .DisclosureIndicator : .None
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedDesigner = DataController.data.designerForIndex(indexPath.row)
        if selectedDesigner?.hasProducts == true {
            performSegueWithIdentifier("ShowDetail", sender: self)
        } else {
            alert("No Products", message: "The selected designer doesn't currently have any products.") { (_) in
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
    }
    
    //MARK - Actions
    
    func tappedHeartAtIndex(index: Int?) {
        guard let index = index, let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0)) as? DesignerCell else {
            //cell offscreen, shouldnt happen if its being tapped
            return
        }
        let wasLiked = cell.likeButton.liked
        cell.likeButton.liked = !wasLiked
        
        if let designer = DataController.data.designerForIndex(index) {
            DataController.data.toggleDesignerLike(designer, liked: !wasLiked)
        }
    }
    
    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        switch segue.identifier {
        case "ShowDetail"?:
            let dest = segue.destinationViewController as! DesignerDetailViewController
            dest.designer = selectedDesigner
        default: break
        }
    }    
}
