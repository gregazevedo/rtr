//
//  AppDelegate.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/9/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window?.tintColor = UIColor.coolDarkGrayColor()
        if let root = window?.rootViewController as? UITabBarController,
            let nav = root.viewControllers?[0] as? UINavigationController,
            let vc = nav.viewControllers[0] as? DesignersViewController {
            loadData(topView: vc.tableView)
        }
        return true
    }
    
    func loadData(topView tableView: UITableView) {
        let indicator = ActivityIndicator(superView: tableView)
        indicator.startAnimating()
        Networking.GetDesigners(.Accessories) { () in
            dispatch_async(dispatch_get_main_queue()) {
                tableView.reloadData()
            }
            Networking.GetDesigners(.Dresses) { () in
                DataController.data.unarchiveLikes() //requires designers already loaded from api
                dispatch_async(dispatch_get_main_queue()) {
                    tableView.reloadData()
                }
                Networking.GetProducts() { () in
                    dispatch_async(dispatch_get_main_queue()) {
                        indicator.stopAnimating()
                        tableView.reloadData()
                    }
                }
            }
        }
    }
    
    func applicationWillResignActive(application: UIApplication) {
        DataController.data.archiveLikes()
    }
}

