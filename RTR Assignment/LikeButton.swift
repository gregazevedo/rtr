//
//  LikeButton.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class LikeButton: UIButton {
    init() {
        super.init(frame:CGRectZero)
        //.AlwaysTemplate rendering mode to tint image
        setImage(UIImage(named: "selectedHeart")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Selected)
        setImage(UIImage(named: "unselectedHeart")?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var liked: Bool {
        get {
            return selected
        }
        set {
            selected = newValue
        }
    }
}
