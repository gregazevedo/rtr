//
//  DataController.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

class DataController {
    private var designers: [Designer] = []
    private var likedDesignerNames: [String] = []
    private var likedDesigners: [Designer] = []
    static var data = DataController()
    
    func toggleDesignerLike(designer: Designer, liked: Bool) {
        designer.liked = liked
        if designer.liked {
            likedDesignerNames.append(designer.name!)
            if let idx = likedDesigners.indexOf({$0.name?.lowercaseString > designer.name?.lowercaseString}) {
                likedDesigners.insert(designer, atIndex: idx)
            } else {
                likedDesigners.append(designer)
            }
        } else {
            if let idx = likedDesignerNames.indexOf(designer.name!) {
                likedDesignerNames.removeAtIndex(idx)
            }
            if let idx = likedDesigners.indexOf({$0.name == designer.name}) {
                likedDesigners.removeAtIndex(idx)
            }
        }
    }
    
    func insertDesigners(designerNames: [String]) {
        for name in designerNames {
            let designer = Designer(name: name)
            designers.append(designer)
        }
        designers.sortInPlace({ $0.name!.lowercaseString < $1.name!.lowercaseString })
    }
    
    func insertProductsFromJSON(productsList: [[String:AnyObject]]) {
        for productDict in productsList {
            if let name = productDict["designer"] as? String,
                let type = productDict["type"] as? String,
                let designer = designerForName(name) {
                //ignore the item if the designer was not in designers lists
                var product: Product?
                switch type {
                case "Accessory":
                    product = Accessory(designer: designer, data: productDict)
                case "Dress":
                    product = Dress(designer: designer, data: productDict)
                default: break
                }
                designer.addProduct(product)
            }
        }
    }
    
    func clear() {
        designers = []
        likedDesigners = []
        likedDesignerNames = []
    }
    
    //MARK: DesignersVC Data
    
    func designerForName(name: String) -> Designer? {
        for designer in designers {
            if designer.name == name {
                return designer
            }
        }
        return nil
    }
    
    func designerForIndex(index: Int) -> Designer? {
        guard index < numDesigners() else {
            return nil
        }
        return designers[index]
    }
    
    func numDesigners() -> Int {
        return designers.count
    }
    
    //MARK: MyHeartsVC Data
    
    func numLikedDesigners() -> Int {
        return likedDesigners.count
    }
    
    func likedDesignerForIndex(index: Int) -> Designer? {
        guard index < numLikedDesigners() else {
            return nil
        }
        return likedDesigners[index]
    }
    
    //MARK: Persisting likes
    
    func unarchiveLikes() {
        guard let path = ArchiveFilePath,
            let names = NSKeyedUnarchiver.unarchiveObjectWithFile(path) as? [String] else {
                return
        }
        likedDesignerNames = names
        for name in likedDesignerNames {
            if let idx = designers.indexOf({ $0.name == name }) {
                let designer = designers[idx]
                designer.liked = true
                likedDesigners.append(designer)
            }
        }
    }
    
    func archiveLikes() {
        guard let path = ArchiveFilePath else {
            return
        }
        NSKeyedArchiver.archiveRootObject(likedDesignerNames, toFile: path)
    }
}
