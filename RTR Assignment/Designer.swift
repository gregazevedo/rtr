//
//  Designer.swift
//  RTR Assignment
//
//  Created by Gregory Azevedo on 6/11/16.
//  Copyright © 2016 Gregory Azevedo. All rights reserved.
//

import UIKit

enum DesignerType: String {
    case Dresses
    case Accessories
}

class Designer {
    var name: String?
    private var dresses: [Dress] = []
    private var accessories: [Accessory] = []
    var liked: Bool = false
    var hasProducts: Bool {
        return !(accessories.isEmpty && dresses.isEmpty)
    }
    
    init(name: String) {
        self.name = name
    }
    
    func addProduct(product: Product?) {
        if let dress = product as? Dress {
            dresses.append(dress)
        } else if let accessory = product as? Accessory {
            accessories.append(accessory)
        }
    }
    
    func numProducts() -> Int {
        return dresses.count + accessories.count
    }
    
    func productForIndex(index: Int) -> Product? {
        //display dresses first then accessories
        return index < dresses.count ? dresses[index] : accessories[index - dresses.count]
    }
    
}
